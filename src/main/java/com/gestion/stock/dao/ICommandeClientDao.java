package com.gestion.stock.dao;

import com.gestion.stock.entites.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient> {

}
