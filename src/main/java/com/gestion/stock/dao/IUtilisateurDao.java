package com.gestion.stock.dao;

import com.gestion.stock.entites.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
