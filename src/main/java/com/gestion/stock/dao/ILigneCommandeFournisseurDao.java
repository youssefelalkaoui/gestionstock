package com.gestion.stock.dao;

import com.gestion.stock.entites.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
