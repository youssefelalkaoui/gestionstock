package com.gestion.stock.dao;

import com.gestion.stock.entites.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur> {

}
