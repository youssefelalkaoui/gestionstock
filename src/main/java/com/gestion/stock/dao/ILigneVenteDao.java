package com.gestion.stock.dao;

import com.gestion.stock.entites.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
