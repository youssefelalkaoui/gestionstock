package com.gestion.stock.dao;

import com.gestion.stock.entites.Client;

public interface IClientDao extends IGenericDao<Client> {

}
