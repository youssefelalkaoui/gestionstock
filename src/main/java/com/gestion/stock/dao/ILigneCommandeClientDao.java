package com.gestion.stock.dao;

import com.gestion.stock.entites.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {
      
}
