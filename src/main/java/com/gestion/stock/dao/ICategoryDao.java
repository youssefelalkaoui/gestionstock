package com.gestion.stock.dao;

import com.gestion.stock.entites.Category;

public interface ICategoryDao extends IGenericDao<Category> {

}
