package com.gestion.stock.dao;

import com.gestion.stock.entites.Article;

public interface IArticleDao extends IGenericDao<Article> {

}
