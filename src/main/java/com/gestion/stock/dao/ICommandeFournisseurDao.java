package com.gestion.stock.dao;

import com.gestion.stock.entites.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur>{

}
