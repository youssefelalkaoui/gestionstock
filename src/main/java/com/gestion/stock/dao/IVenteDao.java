package com.gestion.stock.dao;

import com.gestion.stock.entites.Vente;

public interface IVenteDao extends IGenericDao<Vente> {

}
