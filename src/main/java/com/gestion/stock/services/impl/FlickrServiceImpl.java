package com.gestion.stock.services.impl;

import java.io.InputStream;

import com.gestion.stock.dao.IFlickrDao;
import com.gestion.stock.services.IflickrService;


public class FlickrServiceImpl implements IflickrService{
	
	private IFlickrDao dao;
	
	
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}


	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		return dao.savePhoto(photo,title);
	}

}
